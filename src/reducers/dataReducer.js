import * as types from '../actions/data';

const initialState = {
  data: [],
  dataFetched: false,
  isFetching: false,
  error: false,
};

export default function dataReducer(state = initialState, action) {
  switch (action.type) {
    case types.FETCHING_DATA:
      return {
        ...state,
        data: [],
        isFetching: true,
      };
    case types.CANCEL_FETCHING_DATA:
      return {
        ...state,
        dataFetched: false,
        isFetching: false,
      };
    case types.FETCHING_DATA_SUCCESS:
      return {
        ...state,
        isFetching: false,
        dataFetched: true,
        data: action.data,
      };
    case types.FETCHING_DATA_FAILURE:
      return {
        ...state,
        dataFetched: false,
        isFetching: false,
        error: true,
      };
    default:
      return state;
  }
}
