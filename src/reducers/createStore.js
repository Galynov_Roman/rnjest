import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { persistStore, persistReducer } from 'redux-persist';

import reducers from './createReducer';
import reduxPersist from '../config/reduxPersist';
import dataSaga from '../sagas/dataSaga';

const persistConfig = reduxPersist.storeConfig;
const persistedReducer = persistReducer(persistConfig, reducers);
const sagaMiddleware = createSagaMiddleware();

export default function configureStore() {
  const store = createStore(persistedReducer, applyMiddleware(sagaMiddleware));
  sagaMiddleware.run(dataSaga);
  const persistor = persistStore(store);
  return { store, persistor };
}
