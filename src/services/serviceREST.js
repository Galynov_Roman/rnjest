import { create } from 'apisauce';
import { REQUEST_URL } from '../constants/requestUrl';

export const api = create({
  baseURL: REQUEST_URL,
  headers: {
    Accept: 'application/json',
    'Content-type': 'application/json',
  },
  timeout: 5000,
});

export const setTokenToHeaders = (token) => {
  api.setHeaders({
    Authorization: `${token}`,
  });
};

export const getPeople = () => new Promise((resolve, reject) => {
  const people = [
    { name: 'Nader', age: 36 },
    { name: 'Amanda', age: 24 },
    { name: 'Jason', age: 44 },
  ];
  setTimeout(() => resolve(people), 2000);
});

// export const logoutUser = data => new Promise((resolve, reject) => {
//   api.post(`${REQUEST_URL}api/Users/logout`, data)
//     .then((response) => {
//       if (response.ok) {
//         if (response.status === 204) {
//           setTokenToHeaders(null);
//           console.warn('logout successful');
//           resolve();
//         }
//       } else {
//         reject();
//       }
//     })
//     .catch((error) => {
//       console.warn('logoutUser catch error:', error);
//       reject(error);
//     });
// });
