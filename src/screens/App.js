import * as React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { createStackNavigator } from 'react-navigation';
import configureStore from '../reducers/createStore';
import LauncheScreen from '../screens/LauncheScreen';
import SecondScreen from '../screens/SecondScreen';

const { store, persistor } = configureStore();

const RootStack = createStackNavigator({
  Home: {
    screen: LauncheScreen,
    title: 'Home Screen',
  },
  SecondScreen: {
    screen: SecondScreen,
    title: 'Second Screen',
  },
});

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <RootStack />
        </PersistGate>
      </Provider>
    );
  }
}

export default App;
