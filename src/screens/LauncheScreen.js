import * as React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  StatusBar,
  StyleSheet,
  TouchableHighlight,
  TouchableOpacity,
} from 'react-native';

import { connect } from 'react-redux';
import { fetchData } from '../actions/data';
import { colors } from '../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: colors.white,
  },
  mainContent: {
    flex: 1,
  },
});

class LauncheScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerTitle: 'Home Screen',
    headerTitleStyle: styles.headerTitleStyle,
    headerRight: (
      <TouchableOpacity
        style={styles.headerLeft}
        onPress={() => navigation.navigate('SecondScreen')}
      >
        <Text>Second</Text>
      </TouchableOpacity>
    ),
  });

  static propTypes = {
    data: PropTypes.object,
    getData: PropTypes.func,
  };
  static defaultProps = {
    data: [],
    getData: () => {},
  };

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount() {
  }

  render() {
    const {
      data,
      getData,
    } = this.props;
    return (
      <View style={styles.container}>
        <StatusBar hidden />
        <View style={styles.container}>
          <Text>RNJest Example</Text>
          <TouchableHighlight onPress={() => getData()}>
            <Text>Load Data</Text>
          </TouchableHighlight>
          <View style={styles.mainContent}>
            {
              data.data.length ? (
                data.data.map((person, i) => (
                  <View key={i} >
                    <Text>Name: {person.name}</Text>
                    <Text>Age: {person.age}</Text>
                  </View>))
              ) : null
            }
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  data: state.data,
});

const mapDispatchToProps = dispatch => ({
  getData: () => dispatch(fetchData()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LauncheScreen);
