import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';

import { connect } from 'react-redux';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
});

class SecondScreen extends React.Component {

  static navigationOptions = ({ navigation }) => ({
    headerTitle: 'Second Screen',
    headerTitleStyle: styles.headerTitleStyle,
    headerLeft: (
      <TouchableOpacity
        style={styles.headerLeft}
        onPress={() => navigation.navigate('Home')}
      >
        <Text>Back</Text>
      </TouchableOpacity>
    ),
  });

  static propTypes = {
  };
  static defaultProps = {
  };

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Second Screen</Text>
      </View>
    );
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(SecondScreen);
