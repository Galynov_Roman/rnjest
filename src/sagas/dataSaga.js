import { put, takeEvery } from 'redux-saga/effects';
import * as types from '../actions/data';
import { getPeople } from '../services/serviceREST';

function* fetchData(action) {
  console.warn('enter saga');
  try {
    const data = yield getPeople();
    yield put({ type: types.FETCHING_DATA_SUCCESS, data });
  } catch (e) {
    yield put({ type: types.FETCHING_DATA_FAILURE });
  }
}

function* dataSaga() {
  yield takeEvery(types.FETCHING_DATA, fetchData);
}

export default dataSaga;
