export const colors = {
  white: '#fff',
  black: '#000',
  lightBlack: '#333333', // rgb 51 51 51
  dark: '#1E363D', // rgb 30 54 61
  darkGreyBlue: '#30414B', // rgb 48 65 75
  darkGreyBlueTwo: '#374855', // rgb 55 72 85
  BFDashboardColor: '#fff',
  blue: '#4990e2',
  lightBlue: '#609CE0',
  dullBlue: '#4570A1', // rgb 69 112 161
  red: '#d50000',
  grey: '#F8F8F8',
  lightGrey: '#F2F2F2',
  middleGrey: '#8B9AA0',
  darkGrey: '#AFAFAF',
  darkGrey2: '#30414B',
  slateGrey: '#5B595F', // rgb 91 89 95
  steelGrey: '#79888F', // 121 136 143
  purpleyGrey: '#858585', // 133 133 133
  transparent: 'transparent',

  activeBorderColor: '#3591d0',
  inactiveBorderColor: '#4A90E2',

  loginBtnBgColor: '#4A90E2',
  navigationTextColor: '#B2C5D8',

  activeAppElementColor: '#1E363D',
  inactiveAppElementColor: '#4A90E2',

  infoPanelBgColor: '#fbc2cd',
  infoPanelTextColor: '#db2346',

  shadowPopupColor: '#D6D6D6',
};
